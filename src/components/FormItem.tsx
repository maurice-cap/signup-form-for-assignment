import React from 'react';
import './FormItem.scss';

interface FormItem {
  field: string;
  label: string;
  children: React.ReactNode;
  helpMessage?: string;
}

const FormItem = ({ 
  field, 
  label,
  children, 
  helpMessage
}: FormItem) => {

  return(
    <React.Fragment>
      <label htmlFor={field}>{ label }</label>
      { React.Children.map(children, child => {
          if (!React.isValidElement(child)) {
            return child
          }
          let elementChild: React.ReactElement = child
          return React.cloneElement(elementChild, { field });
        })
      }
      { helpMessage &&
        <div className="helpMessage"><p>{ helpMessage }</p></div>
      }
    </React.Fragment>
  );

}

export default FormItem;