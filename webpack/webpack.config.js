const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const SRC_DIR = path.resolve(__dirname, '../src');
const ASSET_PATH = process.env.ASSET_PATH || '/';

module.exports = {
  entry: `${ SRC_DIR }/index.tsx`,
  target: 'web',
  output: {
    path: path.resolve(__dirname, '../build'),
    publicPath: ASSET_PATH, 
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
    alias: {
      '~': path.resolve(__dirname, SRC_DIR),
      Components: path.resolve(SRC_DIR, './components'),
      Pages: path.resolve(SRC_DIR, './pages'),
      Forms: path.resolve(SRC_DIR, './forms')
    }
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        loader: 'awesome-typescript-loader',
      }, {
        test: /\.(sa|sc|c)ss$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
            'css-loader',
            'sass-loader',
        ],
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '../public', 'index.html'),
    }),
    new MiniCssExtractPlugin({
      filename: './src/style.css',
    })
  ],
};
