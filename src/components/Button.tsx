import React from 'react';
import './Button.scss'

interface Button {
  children: string;
  disabled?: boolean;
  htmltype?: string;
  onClick?: React.MouseEventHandler<HTMLElement>
}

const Button = ({ children, ...rest }: Button) => {

  return(
    <button {...rest}>
      { children }
    </button>
  );
}

export default Button;