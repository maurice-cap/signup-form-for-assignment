context('Integration tests', () => {

  beforeEach(() => {
    cy.visit('localhost:1337')
    cy.get('[data-test-id="signupButton"]')
      .click();
  })

  it('should open & close a modal', () => {
    cy.get('.modalContentWrap')
      .should('be.visible');
    cy.get('[data-test-id="closeModalX"]')
      .click();
    cy.get('#modalContentWrap')
      .should('not.be.visible');
  })

  it('should open & close a modal by clicking outside the modal', () => {
    cy.get('.modalContentWrap')
      .should('be.visible');
    cy.get('.modalBackground')
      .click({ force: true });
    cy.get('#modalContentWrap')
      .should('not.be.visible');
  })

  it('should open a modal and show form errors after submit', () => {
    cy.get('[data-test-id="signupActionButton"]')
      .click();
    cy.get('.formError')
      .should('be.visible')
      .find('li')
      .should('have.length', 4)
  });

  it('should prevent posting form data with only a firstname', () => {
    cy.get('#firstName')
      .type('test')
    cy.get('[data-test-id="signupActionButton"]')
      .click();
    cy.get('.formError')
      .should('be.visible')
      .find('li')
      .should('have.length', 3)
  });
  
  it('should prevent posting form data with only a first- and lastname', () => {
    cy.get('#firstName')
      .type('test')
    cy.get('#lastName')
      .type('another')
    cy.get('[data-test-id="signupActionButton"]')
      .click();
    cy.get('.formError')
      .should('be.visible')
      .find('li')
      .should('have.length', 2)
  });

  it('should prevent posting form data with only a first- and lastname and malformed email address', () => {
    cy.get('#firstName')
      .type('test')
    cy.get('#lastName')
      .type('another')
    cy.get('#email')
      .type('mymail')
    cy.get('[data-test-id="signupActionButton"]')
      .click();
    cy.get('.formError')
      .should('be.visible')
      .find('li')
      .should('have.length', 2)
  });

  it('should prevent posting form data without a password', () => {
    cy.get('#firstName')
      .type('test')
    cy.get('#lastName')
      .type('another')
    cy.get('#email')
      .type('mymail@mail.com')
    cy.get('[data-test-id="signupActionButton"]')
      .click();
    cy.get('.formError')
      .should('be.visible')
      .find('li')
      .should('have.length', 1)
  });

  it('should prevent posting form data with firstname in the password', () => {
    cy.get('#firstName')
      .type('test')
    cy.get('#lastName')
      .type('another')
    cy.get('#email')
      .type('mymail@mail.com')
    cy.get('#password')
      .type('myTestPassword')
    cy.get('[data-test-id="signupActionButton"]')
      .click();
    cy.get('.formError')
      .should('be.visible')
      .find('li')
      .should('have.length', 1)
  });

  it('should prevent posting form data with lastname in the password', () => {
    cy.get('#firstName')
      .type('test')
    cy.get('#lastName')
      .type('another')
    cy.get('#email')
      .type('mymail@mail.com')
    cy.get('#password')
      .type('myAnotherPassword')
    cy.get('[data-test-id="signupActionButton"]')
      .click();
    cy.get('.formError')
      .should('be.visible')
      .find('li')
      .should('have.length', 1)
  });

  it('should prevent posting form data with a short password', () => {
    cy.get('#firstName')
      .type('test')
    cy.get('#lastName')
      .type('another')
    cy.get('#email')
      .type('mymail@mail.com')
    cy.get('#password')
      .type('pass')
    cy.get('[data-test-id="signupActionButton"]')
      .click();
    cy.get('.formError')
      .should('be.visible')
      .find('li')
      .should('have.length', 1)
  });

  it('should prevent posting form data without any capital letters in password', () => {
    cy.get('#firstName')
      .type('test')
    cy.get('#lastName')
      .type('another')
    cy.get('#email')
      .type('mymail@mail.com')
    cy.get('#password')
      .type('mylowercasepass')
    cy.get('[data-test-id="signupActionButton"]')
      .click();
    cy.get('.formError')
      .should('be.visible')
      .find('li')
      .should('have.length', 1)
  });
});