import React from 'react';
import MainPage from 'Pages/Main'
import './App.scss'

const App = () => {
  return(
    <MainPage />
  );
}

export default App;