export default [{
  key: 'firstName', 
  label: 'Firstname',
  input: {
    required: true, 
    errorMessage: 'Firstname is required',
  }
}, {
  key: 'lastName', 
  label: 'Lastname',
  input: {
    required: true, 
    errorMessage: 'Lastname is required',
  }
}, {
  key: 'email', 
  label: 'Email',
  input: {
    type: 'email',
    required: true,
    errorMessage: 'E-mail is required and should be a valid email address', 
  }
}, {
  key: 'password', 
  label: 'Password',
  input: {
    type: 'password',
    pattern: '(?=.*[a-z])(?=.*[A-Z]).{8,}',
    minLength: 8,
    required: true,
    errorMessage: 'Password hould contain at least 8 characters, upper- and lowercase characters and cannot contain first- or lastname',
  }
}]