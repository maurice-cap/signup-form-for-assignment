const getField = (fields: any[], value: string, key: string = 'key') => {
  const field: any = fields.find((item) => { 
    if(item[key] == value){
      return item;
    }
  })
  return field;
}

export {
  getField
}