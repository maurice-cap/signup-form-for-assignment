import React, { useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';
import './Modal.scss';

interface ModalProps {
  data: React.ReactNode;
  visibility: boolean;
  dispatchVisibility: Function;
  title?: string;
  width?: string;
}

const Modal = ({ 
  data, 
  visibility, 
  dispatchVisibility,
  title = 'New Modal', 
  width = '500px'
}: ModalProps) => {
  
  return createPortal(
    <React.Fragment>
      { visibility &&
        <>
          <div className="modalBackground" onClick={() => dispatchVisibility(false)}></div> 
          <section className="modalContentWrap" style={{ width: width }}>
            <header>
              <h3>{ title }</h3>
              <span 
                data-test-id="closeModalX"
                onClick={() => dispatchVisibility(false)}
              >
                X
              </span>
            </header>
            <div className="modalContent">
              { data }
            </div>
          </section>
        </>
      }
    </React.Fragment>
  , document.body);

}

export default Modal;