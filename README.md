# Signup form assignment

Signup form assesment for Rabobank. Please use the button on the top right or text link to open the registration form. I decided to use a modal because a signup form is, imho in most cases, irrelevant to the context of the main page and therefore should be displayed as such.

## Setup
1. Install dependencies with `yarn install` or `npm install`

### Build for production
To build the application for production run `yarn build`. The app will be deployed in the `/build` folder in the root of the project.

### Build for development
To start the application in development mode run `yarn start` or `yarn dev`. The app will be accessible at localhost:1337

### Tests
To execute unit- or integration tests first make sure the app is accessible at localhost:1337 and then run `yarn test:unit` or `yarn test:integrations` respectively.

## Known issues
- Add typing to `useRef` hook in `SignupForm.tsx`
- CORS issues with fetching data from/to postman-echo.com
- The use of `@import` instead of `@use` for importing Sass file
- Password validation should be in one regex