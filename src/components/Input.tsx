import React from 'react';
import './Input.scss'

interface FormInput {
  type?: string;
  field?: string;
  required?: boolean;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  minLength?: number
  pattern?: string
}

const Input = ({
  field,
  type = 'text', 
  required = false, 
  ...rest
}: FormInput) => {

  return(
    <input 
      { ...rest }
      name={field}
      id={field}
      type={type}
      required={required}
    />
  );

}

export default Input;