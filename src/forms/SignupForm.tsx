import React, { useState, useRef, useEffect } from 'react';
import { Button, FormError, FormItem, Input } from 'Components';
import { getField } from './helpers';
import formFields from './formFields'
import './SignupForm.scss';

const SignupForm = () => {

  const form = useRef(null); // Should be of type HTMLFormElement, but results in issues
  const [loading, setLoading] = useState<boolean>(false);
  const [values, setValues] = useState<any>({});
  const [errors, setErrors] = useState<any[]>([]);
  const [response, setResponse] = useState<any[]>([]);

  // If the POST call is resolved, try GET
  useEffect(() => {
    if(!response.length || response.length === 2){
      return;
    }
    (async () => {
      try{
        const res = await fetch('http://postman-echo.com/delay/4', { method: 'GET' });
        setResponse([...response, { type: 'success', message: 'Data received from server' }]);
      }catch(e){
        setResponse([...response, { type: 'error', message: e.toString() }]);
      }
      setLoading(false);
    })();    
  }, [response])
  
  
  /*
  * Using the Fetch API to make POST or GET calls to postman-echo.com result in CORS issues 
  * due to the destination not accepting requests from origin. 
  */
  const processFormData = async () => {
    try{
      const res = await fetch('http://postman-echo.com/post', { method: 'POST' });
      setResponse([...response, { type: 'success', message: 'Account successfully created' }]);
    } catch(e) {
      setResponse([...response, { type: 'error', message: e.toString() }]);
    }
  }
  
  /*
  * @desc use HTML5 Validation API to determine a) whether the form is validated and b) 
  * to collect all fields that didn't validate. All invalid fields are added to the error 
  * state and displayed below the inputs.
  */
  const validateFormData = async (event: React.FormEvent) => {    
    event.preventDefault();
    const formElement = form.current;
    if(!formElement.checkValidity()){
      var formErrors: Object[] = [];
      [...formElement].forEach((inputElement) => {
        const { valid }: { valid: boolean } = inputElement.validity;
        if(!valid){
          formErrors = [...formErrors, { 
            field: inputElement.id, 
            message: getField(formFields, inputElement.id).input.errorMessage }]
        }
      });
      setErrors(formErrors);
    }else{
      // since the check for the first- and lastname is not specified in the 
      // config, do an additional check
      if(
        values['password'].toLowerCase().includes(values['firstName'].toLowerCase()) || 
        values['password'].toLowerCase().includes(values['lastName'].toLowerCase())
      ){
        setErrors([{ field: 'password', message: getField(formFields, 'password').input.errorMessage }]);
      }else{
        setLoading(true);
        setErrors([]);
        await processFormData();
      }
    }
  }

  return(
    <form ref={form}>
      <>
        { formFields.map(({ 
            key, 
            label, 
            input: { 
              type, 
              required, 
              minLength, 
              pattern 
            } 
          }) => {
            return(
              <FormItem
                key={key}
                field={key}
                label={label}
              >
                <Input 
                  type={type}
                  required={required}
                  minLength={minLength}
                  pattern={pattern}
                  onChange={({ target: { name, value } }) => {
                    setValues({...values, [name]: value })
                  }}
                /> 
              </FormItem>
            );
        })}
      </>
      { errors.length > 0 && 
          <FormError errors={errors} />
      }
      { loading &&
        <div className="loadingIndicator">
          <p>Processing data...</p>
        </div>
      }
      { response.length > 0 && !loading &&
        <div className="responseContainer">
          <ul>
            { response.map(({ type, message }) => {
              return(<li className={`response ${ type }`}>{ message }</li>);
            })}
          </ul>
        </div>
      }
      <Button 
        data-test-id="signupActionButton"
        disabled={loading}
        htmltype="submit"
        onClick={validateFormData}
      >
        Create account 
      </Button>
    </form>
  );
}

export default SignupForm;