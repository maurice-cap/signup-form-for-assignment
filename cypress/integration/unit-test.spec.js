context('Unit tests', () => {

  beforeEach(() => {
    cy.visit('localhost:1337')
  })

  it('has a signup button', () => {
    cy.get('[data-test-id="signupButton"]')
      .should('be.visible');
  })
  
  it('has a signup link', () => {
    cy.get('[data-test-id="signupLink"]')
      .should('be.visible');
  })
  
  it('has a modal', () => {
    cy.get('[data-test-id="signupLink"]')
      .should('be.visible')
      .click();
    cy.get('.modalContent')
      .should('be.visible');
  })
  
  it('should have a firstname field', () => {
    cy.get('[data-test-id="signupButton"]')
      .click();
    cy.get('#firstName')
      .should('be.visible');
  })
  
  it('should have a lastname field', () => {
    cy.get('[data-test-id="signupButton"]')
      .click();
    cy.get('#lastName')
      .should('be.visible');
  })
  
  it('should have an email field', () => {
    cy.get('[data-test-id="signupButton"]')
      .click();
    cy.get('#email')
      .should('be.visible');
  })

  it('should have a password field', () => {
    cy.get('[data-test-id="signupButton"]')
      .click();
    cy.get('#password')
      .should('be.visible');
  })

  it('should have a submit button', () => {
    cy.get('[data-test-id="signupButton"]')
      .click();
    cy.get('[data-test-id="signupActionButton"]')
      .should('be.visible');
  })
});