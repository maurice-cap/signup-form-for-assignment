import React, { useState } from 'react';
import { Button, Modal } from 'Components';
import SignupForm from 'Forms/SignupForm';
import './index.scss';

const MainPage = () => {

  const [showModal, setShowModal] = useState<boolean>(false);

  return(
    <div>
      <header>
        <h1>Demo application for Rabobank</h1>
        <div className="headerControls">
          <Button
            data-test-id="signupButton"
            onClick={() => setShowModal(!showModal)}
          >
            Signup Form
          </Button>
        </div>
      </header>
      <main>
        <section>
          <h2>Access denied</h2>
          <p>Please <a data-test-id="signupLink" onClick={() => setShowModal(!showModal)}>signup for an account</a> to access the information on this page.</p>
        </section>
      </main>
      <Modal
        data={<SignupForm />}
        title="Signup form"
        visibility={showModal}
        dispatchVisibility={setShowModal}
      />
    </div>
  );
}

export default MainPage;