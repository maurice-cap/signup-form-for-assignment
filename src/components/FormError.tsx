import React from 'react';
import './FormError.scss';

interface FormError {
  errors: any[]
}

const FormError = ({ errors }: FormError) => {

  return(
    <div className="formError">
      <ul>
        { errors.map(({ message }, index: number) => {
          return <li key={`formError-${ index }`}>{ message }</li>
        })}
      </ul>
    </div>
  );

}

export default FormError;